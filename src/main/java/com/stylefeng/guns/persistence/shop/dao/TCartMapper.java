package com.stylefeng.guns.persistence.shop.dao;

import com.stylefeng.guns.persistence.shop.model.TCart;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TCartMapper extends BaseMapper<TCart> {

}
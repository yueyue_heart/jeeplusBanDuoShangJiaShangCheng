/**
 * 初始化通知详情对话框
 */
var [entityClass]InfoDlg = {
    [lowerentity]InfoData : {}
};

/**
 * 清除数据
 */
[entityClass]InfoDlg.clearData = function() {
    this.[lowerentity]InfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
[entityClass]InfoDlg.set = function(key, val) {
    this.[lowerentity]InfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
[entityClass]InfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
[entityClass]InfoDlg.close = function() {
    parent.layer.close(window.parent.[entityClass].layerIndex);
}

/**
 * 收集数据
 */
[entityClass]InfoDlg.collectData = function() {
    this.set('id').set('title').set('content');
}

/**
 * 提交添加
 */
[entityClass]InfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/[lowerentity]/add", function(data){
        Feng.success("添加成功!");
        window.parent.[entityClass].table.refresh();
        [entityClass]InfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.[lowerentity]InfoData);
    ajax.start();
}

/**
 * 提交修改
 */
[entityClass]InfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/[lowerentity]/update", function(data){
        Feng.success("修改成功!");
        window.parent.[entityClass].table.refresh();
        [entityClass]InfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.[lowerentity]InfoData);
    ajax.start();
}

$(function() {

});

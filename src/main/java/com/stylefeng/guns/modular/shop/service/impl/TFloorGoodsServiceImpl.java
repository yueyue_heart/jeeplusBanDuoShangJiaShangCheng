package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TFloorGoods;
import com.stylefeng.guns.persistence.shop.dao.TFloorGoodsMapper;
import com.stylefeng.guns.modular.shop.service.ITFloorGoodsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TFloorGoodsServiceImpl extends ServiceImpl<TFloorGoodsMapper, TFloorGoods> implements ITFloorGoodsService {
	
}

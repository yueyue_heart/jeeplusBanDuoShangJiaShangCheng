package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TOrderLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单处理历史表 服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITOrderLogService extends IService<TOrderLog> {
	
}

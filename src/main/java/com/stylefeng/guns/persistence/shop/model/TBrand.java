package com.stylefeng.guns.persistence.shop.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 商品类型表
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@TableName("t_brand")
public class TBrand extends Model<TBrand> {

    private static final long serialVersionUID = 1L;

    /**
     * 类型id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 类型名称
     */
	private String name;
	private String img;
    /**
     * 商品类型排序
     */
	private int stat;

	public Long getId() {
		return id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStat() {
		return stat;
	}

	public void setStat(int stat) {
		this.stat = stat;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
